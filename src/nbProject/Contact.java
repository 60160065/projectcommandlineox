package nbProject;

import java.util.*;

public class Contact {
	static Scanner kb = new Scanner(System.in);
	// for login variable
	static String username, password;
	// for check Login(User,Pass)
	static boolean foundID = false, foundPass = false;
	// for create ArrayList list for User list 
	static ArrayList<User> arr = new ArrayList<User>();
	// for variable in edit panel
	static User wait;
	// for add variable and edit variable
	static String addFname, addLname, addUser, addPass, addPhone, editUser,editPass, editFname, editLname, editPhone;
	static double addWeight, addHeight, editWeight, editHeight;

	//for Starter
	public static void main(String[] args) {
		LoadData();
		ShowLogin();
	}

	//for load data login and get data
	public static void LoadData() {
		arr.add(new User("Payut", "Jurnangkarn", "admin", "admin","0888888888", 180, 80));
		arr.add(new User("Phatcharapol", "Saenrang", "ace", "ace","0999999999", 175, 55));
	}

	// for Login panel
	public static void ShowLogin() {
		System.out.println("Please input your username and password");
		InputLogin();
		CheckLogin(username, password);
	}
	
	// for input Login 
	public static void InputLogin() {
		System.out.print("Username : ");
		username = kb.next();
		System.out.print("Password : ");
		password = kb.next();
	}

	//for check user and pass
	public static void CheckLogin(String user, String password) {
        for (User a : arr) {
            if (user.equals(a.user)) {
                foundID = true;
                if (password.equals(a.getPass())) {
                    foundPass = true;
                    break;
                } else {
                    foundPass = false;
                    break;
                }
            } else {
                foundID = false;
            }
        }
		CheckFoundLogin();
	}

	//for check user and pass error
	public static void CheckFoundLogin() {
		if (foundID == true && foundPass == true) {
			ShowMenu();
		} else if (foundID == true && foundPass == false) {
			PrintErrorLogin();
			ShowLogin();
		} else {
			PrintErrorLogin();
			ShowLogin();
		}
	}

	// for Menu panel
	public static void ShowMenu() {
		System.out.println("Menu");
		System.out.println("1. Register");
		System.out.println("2. Edit");
		System.out.println("3. ShowUserList");
		System.out.println("4. Logout");
		System.out.println("5. Exit");
		System.out.println("");
		CheckMenu();
	}

	//for check choose Menu
	public static void CheckMenu() {
		ShowPleaseChoose();
		char input = kb.next().charAt(0);
		switch (input) {
		case '1':
			ShowRegister();
			break;
		case '2':
			ShowEdit();
			break;
		case '3':
			ShowUserList();
			break;
		case '4':
			logOut();
			break;
		case '5':
			sayByebye();
			System.exit(0);
			break;
		default:
			PrintErrorChoiceMenu();
			ShowMenu();
		}
	}

	// for Register panel
	public static void ShowRegister() {
		System.out.println("Register");
		System.out.println("#  back");
		System.out.println("1. Firstname \n2. Lastname \n3. Username \n4. Password \n5. Phone \n6. Weight \n7. Height");
		CheckDetail();
	}

	//for check input Register 
	public static void CheckDetail() {
		ChooseAddFname();
		ChooseAddLname();
		ChooseAddUser();
		ChooseAddPass();
		ChooseAddPhone();
		ChooseAddWeight();
		ChooseAddHeight();
		InputForSave();
	}

	//for check add Firstname
	public static void ChooseAddFname() {
		System.out.print("Firstname : ");
		addFname = kb.next();
		if (addFname.equals("#"))
			ShowMenu();
	}
	
	//for check add Lastname
	public static void ChooseAddLname() {
		System.out.print("Lastname : ");
		addLname = kb.next();
		if (addLname.equals("#"))
			ShowMenu();
	}
	
	//for check add User
	public static void ChooseAddUser() {
		System.out.print("Username : ");
		addUser = kb.next();
		if (addUser.equals("#")) {
			ShowMenu();
		} else {
			for (User a : arr) {
				if (a.getUser().equals(addUser)) {
					ChooseAddUser();
				}
			}
			System.out.println("Username can use.");
		}
	}

	//for check add Pass
	public static void ChooseAddPass() {
		System.out.print("Password : ");
		addPass = kb.next();
		if (addPass.equals("#"))
			ShowMenu();
	}
	
	//for check add Phone
	public static void ChooseAddPhone() {
		try{
			System.out.print("Phone : ");
			addPhone = kb.next();
			if (addPhone.equals("#")) {
				ShowMenu();
			} else if (addPhone.length() == 10) {
				int numPhone = Integer.valueOf(addPhone);
				if(numPhone%2==0&&numPhone-1==9){
					
				}
			} else {
				PrintErrorPhone();
				ChooseAddPhone();
			}
		}catch(Exception e){
			PrintErrorHWPEdit();
			ChooseAddPhone();
		}
	}
	
	//for check add Weight
	public static void ChooseAddWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("Weight : ");
				String str = kb.next();
				if (str.equals("#")) {
					ShowMenu();
					break;
				}
				addWeight = Double.parseDouble(str);
				check = true;
			} catch (Exception e) {
				PrintErrorHWPEdit();
			}
		}
		System.out.println("Weight can use.");
	}

	//for check add Height
	public static void ChooseAddHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("Height : ");
				String str = kb.next();
				if (str.equals("#")) {
					ShowMenu();
					break;
				}
				addHeight = Double.parseDouble(str);
				check = true;
			} catch (Exception e) {
				PrintErrorHWPEdit();
			}
		}
		System.out.println("Height can use.");
	}
	
	//for input choice for save
	public static void InputForSave() {
		System.out.println("(#) back / (Y) save");
		ShowPleaseChoose();
		String input = kb.next();
		CheckSave(input);
	}

	//for check choice for save
	public static void CheckSave(String input) {
		if (input.equals("Y")) {
			arr.add(new User(addFname, addLname, addUser, addPass, addPhone,addWeight, addHeight));
			ShowMenu();
		} else if (input.equals("#")) {
			ShowRegister();
		} else
			InputForSave();
	}
	
	// for get data User login in Edit panel
	public static void ShowEdit() {
	       for (User a : arr) {
	            if (username.equals(a.getPass()) && username.equals(password)) {
	            	wait = new User(a.getF(), a.getL(), a.getUser(), a.getPass(),a.getPhone(), a.getW(), a.getH());
	            	CheckEdit();
	            }
	        }
	}

	//for check input Edit
	public static void CheckEdit() {
		InputChoiceEdit();
		String inputEdit = kb.next();
		if (inputEdit.equals("#")) {
			for (User a : arr) {
				if (a.getUser().equals(wait.user)) {
					a.setF(wait.getF());
					a.setL(wait.getL());
					a.setPass(wait.getPass());
					a.setPhone(wait.getPhone());
					a.setW(wait.getW());
					a.setH(wait.getH());
				}
			}
			ShowMenu();
		} else if (inputEdit.equals("1")) {
			ChooseEditFname();
			CheckEdit();
		} else if (inputEdit.equals("2")) {
			ChooseEditLname();
			CheckEdit();
		} else if (inputEdit.equals("3")) {
			PrintErrorUserChange();
			CheckEdit();
		} else if (inputEdit.equals("4")) {
			ChooseEditPass();
			CheckEdit();
		} else if (inputEdit.equals("5")) {
			ChooseEditPhone();
			CheckEdit();
		} else if (inputEdit.equals("6")) {
			ChooseEditWeight();
			CheckEdit();
		} else if (inputEdit.equals("7")) {
			ChooseEditHeight();
			CheckEdit();
		}else {
			PrintErrInputChoiceEdit();
			CheckEdit();
		}

	}

	//for Edit panel
	public static void InputChoiceEdit() {
		System.out.println("Edit");
		System.out.println("#  back");
		System.out.println("1. Firstname: " + wait.getF() + "\n2. Lastname: "
				+ wait.getL() + "\n3. Username: " + wait.getUser()
				+ "\n4. Password: " + wait.getPass() + "\n5. Phone: "
				+ wait.getPhone() + "\n6. Weight: " + wait.getW()
				+ "\n7. Height: " + wait.getH());
		ShowPleaseChoose();
	}
	
	//for check edit Firstname
	public static void ChooseEditFname() {
		System.out.print("Firstname : ");
		editFname = kb.next();
		if (editFname.equals("#"))
			CheckEdit();
		wait.setF(editFname);
	}

	//for check edit Lastname
	public static void ChooseEditLname() {
		System.out.print("Lastname : ");
		editLname = kb.next();
		if (editLname.equals("#"))
			CheckEdit();
		wait.setL(editLname);
	}
	
	//for check edit Pass
	public static void ChooseEditPass() {
		System.out.print("Password : ");
		editPass = kb.next();
		if (editPass.equals("#"))
			CheckEdit();
		wait.setPass(editPass);
	}
	
	//for check edit Phone
	public static void ChooseEditPhone() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("Phone : ");
				editPhone = kb.next();
				if (editPhone.equals("#")) {
					CheckEdit();
					break;
				}else if (editPhone.length() == 10) {
						int numPhone = Integer.valueOf(editPhone);
						if(numPhone%2==0&&numPhone-1==9){
				
						}
				} else {
					PrintErrorPhone();
					ChooseEditPhone();
				}
				wait.setPhone(editPhone);
				check = true;
			} catch(Exception e){
				PrintErrorHWPEdit();
			}
		}
		System.out.println("Phone can use.");
	}
	
	//for check edit Weight
	public static void ChooseEditWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("Weight : ");
				String str = kb.next();
				if (str.equals("#")) {
					CheckEdit();
					break;
				}
				editWeight = Double.parseDouble(str);
				wait.setW(editWeight);
				check = true;
			} catch (Exception e) {
				PrintErrorHWPEdit();
			}
		}
		System.out.println("Weight can use.");
	}

	//for check edit Height
	public static void ChooseEditHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("Height : ");
				String str = kb.next();
				if (str.equals("#")) {
					CheckEdit();
					break;
				}
				editHeight = Double.parseDouble(str);
				wait.setH(editHeight);
				check = true;
			} catch (Exception e) {
				PrintErrorHWPEdit();
			}
		}
		System.out.println("Height can use.");
	}
	
	// for ShowUserList panel
	public static void ShowUserList() {
		System.out.println("UserList");
		System.out.println("#  back");
		for (int i = 0; i < arr.size(); i++) {
			System.out.println(i + 1 + ". " + arr.get(i).user);
		}
		InputChoiceUserList();
	}

	//for input choice UserList
	public static void InputChoiceUserList() {
		boolean check = false;
		while (!check) {
			try {
				ShowPleaseChoose();
				String inputChoose = kb.next();
				CheckShow(inputChoose);

			} catch (InputMismatchException e) {
				PrintErrorChoiceUserList();
				kb.next();
			}
		}
	}

	//for check input for show Detail
	public static void CheckShow(String inputChoose) {
		try {
			for (int i = 0; i < arr.size(); i++) {
				if (inputChoose.equals("#")) {
					ShowMenu();
					break;
				}
				int input = Integer.parseInt(inputChoose);
				if (input == i + 1) {
					System.out.println("");
					ShowDetail();
					System.out.println(arr.get(i));
					System.out.println("");
					ShowUserList();
					break;
				} else if (input > arr.size()) {
					PrintErrorChoiceUserList();
					ShowUserList();
					break;
				}
			}
			PrintErrorChoiceUserList();
			ShowUserList();
		} catch (Exception e) {
			PrintErrorChoiceUserList();
			ShowUserList();
		}

	}

	//for Detail panel
	public static void ShowDetail() {
		System.out.print("Detail");
	}

	// for logout
	public static void logOut() {
		ShowPleaseChoose();
		ShowLogin();
	}
	
	//for show went exit
	public static void sayByebye() {
		System.out.println("Bye Bye...");
	}
	
	// for show message dialog and error message
	public static void PrintErrorLogin() {
		System.err.println("Error: Username or Password is incorrect!! , Please try again.");
	}

	public static void ShowPleaseChoose() {
		System.out.print("Please input choice : ");
	}

	public static void PrintErrInputChoiceEdit() {
		System.err.println("Error :  Please input choice : (#,1-7)");
	}

	public static void PrintErrorChoiceMenu() {
		System.err.println("Error: Please input number between (1-5)");
	}

	public static void PrintErrorChoiceUserList() {
		System.err.println("Error: It's not choice Please select choice again.");
	}

	public static void showErrorShowInput() {
		System.err.println("Weight must be number.");
	}

	public static void PrintErrorHWPEdit() {
		System.err.println("Error :  Type not matching!!!,Please try again.");
	}

	public static void PrintErrorPhone() {
		System.err.println("Error :  Please input phone number (10) characters.");
	}

	public static void PrintErrorUserChange() {
		System.err.println("Error :  Username can't change!!!");
	}

}

// POJO
class User {
	String user;
	String pass;
	String fname;
	String lname;
	String phone;
	double weight;
	double height;

	public User(String fname, String lname, String user, String pass,String phone, double weight, double height) {
		this.fname = fname;
		this.lname = lname;
		this.user = user;
		this.pass = pass;
		this.phone = phone;
		this.weight = weight;
		this.height = height;
	}

	public String toString() {
		String p1 = "\nFirstname : " + fname;
		String p2 = "\nLastname : " + lname;
		String p3 = "\nUsername : " + user;
		String p4 = "\nPassword : " + pass;
		String p5 = "\nPhone : " + phone;
		String p6 = "\nWeight : " + Double.toString(weight);
		String p7 = "\nHeight : " + Double.toString(height);
		return p1 + p2 + p3 + p4 + p5 + p6 + p7;

	}

	public String getF() {
		return fname;
	}

	public String getL() {
		return lname;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public String getPhone() {
		return phone;
	}

	public Double getW() {
		return weight;
	}

	public Double getH() {
		return height;
	}

	public void setF(String fname) {
		this.fname = fname;
	}

	public void setL(String lname) {
		this.lname = lname;
	}

	public void setU(String user) {
		this.user = user;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setW(double weight) {
		this.weight = weight;
	}

	public void setH(double height) {
		this.height = height;
	}
}
